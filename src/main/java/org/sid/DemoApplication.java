package org.sid;

import org.sid.dao.ContactRepository;
import org.sid.dao.VilleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackages={"org.sid"})
public class DemoApplication implements CommandLineRunner {
	@Autowired
	private ContactRepository contactRepository;

	@Autowired
	private VilleRepository villeRepository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

	}

	@Override
	public void run(String... arg0) throws Exception {

		// DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
		// contactRepository.save(new Contact("lamgnouh", "halima",
		// df.parse("18/11/1989"), 776575675, "okokok"));
		// contactRepository.save(new Contact("laksyer", "salah",
		// df.parse("11/10/1999"), 7765775, "ttt"));
		// contactRepository.save(new Contact("test", "khalid",
		// df.parse("18/11/2009"), 7765756, "popo"));
		//
//		contactRepository.findByClients(5).forEach(c->{
//		System.out.println(c.getNom());
//		});

	}
}
