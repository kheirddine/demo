package org.sid.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	@Autowired
	private UserDetailsService jwtUserDetailsService;
	@Autowired
	private JwtRequestFilter jwtRequestFilter;
	
//	@Override
//	public void configure(WebSecurity web) throws Exception {
//		web.ignoring().antMatchers("/","/error", "/index.html", "/app/**", "/main.js", "/config/**", "/node_modules/**", "/rest/login", "/favicon.ico");
//	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// configure AuthenticationManager so that it knows from where to load
		// user for matching credentials
		// Use BCryptPasswordEncoder
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
 
				http.csrf().disable()
				.authorizeRequests().antMatchers(HttpMethod.GET).permitAll()
				.and().authorizeRequests().antMatchers(HttpMethod.POST).permitAll()
				.and().authorizeRequests().antMatchers(HttpMethod.DELETE).permitAll()
				.and().authorizeRequests().antMatchers(HttpMethod.PUT).permitAll()
				///.and().authorizeRequests().anyRequest().fullyAuthenticated()
			 .and().authorizeRequests().antMatchers("/swagger-ui.html","/webjars/**","/swagger-resources/**","/v2/api-docs","/villesDelete/{id}","/villesUp/{id}"
						,"/villes","/villes","/contactsUp","/contacts","/contactsDelete/{id}","/contactsUp",
						"/articles","/articles/{id}","/articlesDelete/{id}","/articlesUp/{id}",
						"/detailarticledevis","/detailarticledevis/{id}","/detailarticledevisDelete/{id}","/detailarticledevisUp/{id}",
						"/devis","/devis/{id}","/devisDelete/{id}","/devisUp/{id},/chercherDevis",
						"/authenticate","/register","/users/findByEmail" ,"/login")
				.permitAll() 
		 
				.anyRequest().authenticated().and().
				// make sure we use stateless session; session won't be used to
				// store user's state.
				exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		// Add a filter to validate the tokens with every request
				http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http
//			.authorizeRequests()
//			.anyRequest().fullyAuthenticated().and().authorizeRequests().antMatchers("/villes","/contactsUp","/contacts","/contacts/{id}","/villes/{id}","/authenticate","/register","/users/findByEmail").permitAll().and()
//			.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
//			.httpBasic().and()
//			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//			.csrf().disable();
//	}

}
