package org.sid.interfaces;

import java.util.List;

import org.sid.entities.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface IContactService {
	Contact save(Contact Contact);
	void update(Contact Contact);
	void delete(Long id);
	void deleteById(Long id);
	void deleteAll();
	Contact getOne(Long id);
	List<Contact> findAll();
	void deleteAllList(List<Contact> listContacts);
	void saveAllList(List<Contact> listContacts);
	public Long count();
	public Contact findByEmail(String email);
	public Contact findByTypeUser(Integer idType);
	Page<Contact> chercher(String string, PageRequest pageRequest);
	Contact findOne(Long id);
}

