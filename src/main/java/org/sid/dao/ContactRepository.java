package org.sid.dao;

import java.util.List;

import org.sid.entities.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
//@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
	@Query(" select c from Contact c where lower(c.nom) like :x")
	  Page<Contact> chercher(@Param ("x")String mc,Pageable pg);

	  Contact findByUsername(String email);
	
	///@Query(" select c from Contact c where  c.typeUser =:x")
	  Contact findByTypeUser(Integer ins);
	
//	  @Query("select u from Contact u where u.typeUser  =5")
	  List<Contact> findAllByTypeUser(Integer ins);
	
 
 

}
