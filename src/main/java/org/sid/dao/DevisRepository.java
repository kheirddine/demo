package org.sid.dao;

import org.sid.entities.Devis;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DevisRepository  extends JpaRepository<Devis, Long> {
	
	@Query(" select c from Devis c where lower(c.contact.nom) like :x")
	  Page<Devis> chercher(@Param ("x")String mc,Pageable pg);

}
