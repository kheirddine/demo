package org.sid.dao;

import org.sid.entities.DetailArticleDevis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DetailArticleDevisRepository  extends JpaRepository<DetailArticleDevis,Long>{

}
