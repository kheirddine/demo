package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "detail_facture_devis")
public class DetailFactureDevis implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	@Temporal(TemporalType.DATE)
	private Date dateCreation;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "facture")
	private Facture facture;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "devis")
	private Devis devis;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Facture getFacture() {
		return facture;
	}

	public void setFacture(Facture facture) {
		this.facture = facture;
	}

	public Devis getDevis() {
		return devis;
	}

	public void setDevis(Devis devis) {
		this.devis = devis;
	}

	public DetailFactureDevis() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DetailFactureDevis(Date dateCreation, Facture facture, Devis devis) {
		super();
		this.dateCreation = dateCreation;
		this.facture = facture;
		this.devis = devis;
	}

	@Override
	public String toString() {
		return "DetailFactureDevis [id=" + id + ", dateCreation=" + dateCreation + ", facture=" + facture + ", devis="
				+ devis + "]";
	}
	 
	
	
}