package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "article")
public class Article implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	private String codeArticle;
	private String nomArticle;
	private String descriptionArticle;
	private Double prixAchat;
	private Double prixVente;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodeArticle() {
		return codeArticle;
	}
	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}
	public String getNomArticle() {
		return nomArticle;
	}
	public void setNomArticle(String nomArticle) {
		this.nomArticle = nomArticle;
	}
	public String getDescriptionArticle() {
		return descriptionArticle;
	}
	public void setDescriptionArticle(String descriptionArticle) {
		this.descriptionArticle = descriptionArticle;
	}
	public Double getPrixAchat() {
		return prixAchat;
	}
	public void setPrixAchat(Double prixAchat) {
		this.prixAchat = prixAchat;
	}
	public Double getPrixVente() {
		return prixVente;
	}
	public void setPrixVente(Double prixVente) {
		this.prixVente = prixVente;
	}
	public Article() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Article(String codeArticle, String nomArticle, String descriptionArticle, Double prixAchat,
			Double prixVente) {
		super();
		this.codeArticle = codeArticle;
		this.nomArticle = nomArticle;
		this.descriptionArticle = descriptionArticle;
		this.prixAchat = prixAchat;
		this.prixVente = prixVente;
	}
	@Override
	public String toString() {
		return "Article [id=" + id + ", codeArticle=" + codeArticle + ", nomArticle=" + nomArticle
				+ ", descriptionArticle=" + descriptionArticle + ", prixAchat=" + prixAchat + ", prixVente=" + prixVente
				+ "]";
	}
	
	
	
	
	
}