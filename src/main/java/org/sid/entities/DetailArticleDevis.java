package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "detail_article_devis")
public class DetailArticleDevis implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	@Temporal(TemporalType.DATE)
	private Date dateCreation;
	private Double qte;
	private Double prixUnitaire;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "article")
	private Article article;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "devis")
	private Devis devis;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public Double getQte() {
		return qte;
	}
	public void setQte(Double qte) {
		this.qte = qte;
	}
 
	public Double getPrixUnitaire() {
		return prixUnitaire;
	}
	public void setPrixUnitaire(Double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public Devis getDevis() {
		return devis;
	}
	public void setDevis(Devis devis) {
		this.devis = devis;
	}
	public DetailArticleDevis() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DetailArticleDevis(Date dateCreation, Double qte, Double prixUnitaire, Article article, Devis devis) {
		super();
		this.dateCreation = dateCreation;
		this.qte = qte;
		this.prixUnitaire = prixUnitaire;
		this.article = article;
		this.devis = devis;
	}
	@Override
	public String toString() {
		return "DetailArticleDevis [id=" + id + ", dateCreation=" + dateCreation + ", qte=" + qte + ", prixUnitaire=" + prixUnitaire
				+ ", article=" + article + ", devis=" + devis + "]";
	}
 
	
	
	
}