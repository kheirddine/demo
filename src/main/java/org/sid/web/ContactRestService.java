package org.sid.web;

import java.util.List;

import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.sid.entities.JwtRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
///@CrossOrigin(origins = "*")
@CrossOrigin("**")
public class ContactRestService {
	@Autowired
	private ContactRepository contactRepository;

	@RequestMapping(value = "/contacts", method = RequestMethod.GET)
	public List<Contact> getContacts() {
		return contactRepository.findAll();

	}
 
	@RequestMapping(value = "/clientsType/{id}", method = RequestMethod.GET)
	public List<Contact> getClients(@PathVariable Integer id ) {
		return contactRepository.findAllByTypeUser(id);

	}
	
@RequestMapping(value = "/chercherContacts", method = RequestMethod.GET)
	public Page<Contact> chercher(@RequestParam (name="mc",defaultValue="") String mc, 
			@RequestParam(name="page",defaultValue="0") int page,
			@RequestParam(name="size",defaultValue="5") int size){
		return contactRepository.chercher("%"+mc.toLowerCase()+"%", new PageRequest(page, size));

	}
	@RequestMapping(value = "/contacts/{id}", method = RequestMethod.GET)
	public Contact getContact(@PathVariable Long id) {
		return contactRepository.findOne(id);

	}
	@RequestMapping(value = "/contactsType/{id}", method = RequestMethod.GET)
	public Contact getEmployeesByType(@PathVariable Integer id) {
		return contactRepository.findByTypeUser(id );

	}
	@RequestMapping(value = "/contacts", method = RequestMethod.POST)
	public Contact save( @RequestBody Contact c ) {
		
		return contactRepository.save(c);

	}
	//value = "/contacts/{id}",  @PathVariable Long id 
	//@RequestMapping( method = RequestMethod.PUT)
	@RequestMapping(value = "/contactsUp", method = RequestMethod.POST)
	public Contact update(@RequestBody Contact c ) {
		//c.setId(id);
		return contactRepository.save(c);

	}
	@RequestMapping(value = "/contactsDelete/{id}", method = RequestMethod.POST)
	public boolean supprimer(@PathVariable Long id) {
		 try {
			 contactRepository.delete(id);
			 return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 return false;
		}
	

	}
	
@GetMapping(value = "/Connected")
    public ResponseEntity<Contact>  getuserConnected() {
	Contact connectedUser = new Contact();
	connectedUser.setNom(SecurityContextHolder.getContext().getAuthentication().getName());
	return new ResponseEntity<Contact>(connectedUser , HttpStatus.OK);
	}
	
    @PostMapping(value = "/users/findByEmail")
    public ResponseEntity<Contact> getUser(@RequestBody JwtRequest authenticationRequest) {
    	if(authenticationRequest!=null && authenticationRequest.getUsername()!=null && authenticationRequest.getUsername().trim()!="") {
    		System.out.println("Fetching User ");
    		Contact user = contactRepository.findByUsername(authenticationRequest.getUsername());
	        if (user == null) {
	            System.out.println("User not found");
	            return new ResponseEntity<Contact>(HttpStatus.NOT_FOUND);
	        }
	        System.out.println(" get  User " +user.getUsername());
	        return new ResponseEntity<Contact>(user, HttpStatus.OK);
    	}else {
    		System.out.println("User not found");
            return new ResponseEntity<Contact>(HttpStatus.PARTIAL_CONTENT);
    	}
    }

}
