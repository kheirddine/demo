package org.sid.web;

import java.util.List;

import org.sid.dao.DetailArticleDevisRepository;
import org.sid.entities.DetailArticleDevis;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class DetailArticleDevisService {
	DetailArticleDevisRepository detailArticleDevisRepository;
	

	@RequestMapping(value = "/detailarticledevis", method = RequestMethod.GET)
	public List<DetailArticleDevis> getDetailArticleDevis() {
		return detailArticleDevisRepository.findAll();

	}
	 
	@RequestMapping(value = "/detailarticledevis/{id}", method = RequestMethod.GET)
	public DetailArticleDevis getDetailArticleDevis(@PathVariable Long id) {
		return detailArticleDevisRepository.findOne(id);

	}
	@RequestMapping(value = "/detailarticledevis", method = RequestMethod.POST)
	public DetailArticleDevis save(@RequestBody DetailArticleDevis c ) {
		return detailArticleDevisRepository.save(c);

	}
	@RequestMapping(value = "/detailarticledevisUp/{id}", method = RequestMethod.POST)
	public DetailArticleDevis update(@PathVariable Long id ,@RequestBody DetailArticleDevis c ) {
		c.setId(id);
		return detailArticleDevisRepository.save(c);

	}
	@RequestMapping(value = "/detailarticledevisDelete/{id}", method = RequestMethod.POST)
	public boolean supprimer(@PathVariable Long id) {
		 try {
			 detailArticleDevisRepository.delete(id);
			 return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 return false;
		}
	

	}

	

}
