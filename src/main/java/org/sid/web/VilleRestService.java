package org.sid.web;

import java.util.List;

import org.sid.dao.VilleRepository;
import org.sid.entities.Ville;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class VilleRestService {
	@Autowired
	private VilleRepository villeRepository;

	@RequestMapping(value = "/villes", method = RequestMethod.GET)
	public List<Ville> getVilles() {
		return villeRepository.findAll();

	}
	 
	@RequestMapping(value = "/villes/{id}", method = RequestMethod.GET)
	public Ville getVille(@PathVariable Long id) {
		return villeRepository.findOne(id);

	}
	@RequestMapping(value = "/villes", method = RequestMethod.POST)
	public Ville save(@RequestBody Ville c ) {
		return villeRepository.save(c);

	}
	@RequestMapping(value = "/villesUp/{id}", method = RequestMethod.POST)
	public Ville update(@PathVariable Long id ,@RequestBody Ville c ) {
		c.setId(id);
		return villeRepository.save(c);

	}
	@RequestMapping(value = "/villesDelete/{id}", method = RequestMethod.POST)
	public boolean supprimer(@PathVariable Long id) {
		 try {
			 villeRepository.delete(id);
			 return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 return false;
		}
	

	}

}
