package org.sid.web;

import java.util.List;

import org.sid.dao.DevisRepository;
import org.sid.entities.Devis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class DevisRestService {
	
	@Autowired
	private DevisRepository devisRepository;

	@RequestMapping(value = "/devis", method = RequestMethod.GET)
	public List<Devis> getDevis() {
		return devisRepository.findAll();

	}
	@RequestMapping(value = "/chercherDevis", method = RequestMethod.GET)
	public Page<Devis> chercher(@RequestParam (name="mc",defaultValue="") String mc, 
			@RequestParam(name="page",defaultValue="0") int page,
			@RequestParam(name="size",defaultValue="5") int size){
		return devisRepository.chercher("%"+mc.toLowerCase()+"%", new PageRequest(page, size));
	}
	 
	@RequestMapping(value = "/devis/{id}", method = RequestMethod.GET)
	public Devis getDevisById(@PathVariable Long id) {
		return devisRepository.findOne(id);

	}
	@RequestMapping(value = "/devis", method = RequestMethod.POST)
	public Devis save(@RequestBody Devis c ) {
		return devisRepository.save(c);
	
	}
	@RequestMapping(value = "/devisUp/{id}", method = RequestMethod.POST)
	public Devis update(@PathVariable Long id ,@RequestBody Devis c ) {
		c.setId(id);
		return devisRepository.save(c);

	}
	@RequestMapping(value = "/devisDelete/{id}", method = RequestMethod.POST)
	public boolean supprimer(@PathVariable Long id) {
		 try {
			 devisRepository.delete(id);
			 return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 return false;
		}
	

	}

}
