package org.sid.web;

import java.util.List;

import org.sid.dao.ArticleRepository;
import org.sid.entities.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ArticleRestService {
	
	@Autowired
	private ArticleRepository articleRepository;

	@RequestMapping(value = "/articles", method = RequestMethod.GET)
	public List<Article> getArticles() {
		return articleRepository.findAll();

	}
	 
	@RequestMapping(value = "/articles/{id}", method = RequestMethod.GET)
	public Article getArticles(@PathVariable Long id) {
		return articleRepository.findOne(id);

	}
	@RequestMapping(value = "/articles", method = RequestMethod.POST)
	public Article save(@RequestBody Article c ) {
		return articleRepository.save(c);

	}
	@RequestMapping(value = "/articlesUp/{id}", method = RequestMethod.POST)
	public Article update(@PathVariable Long id ,@RequestBody Article c ) {
		c.setId(id);
		return articleRepository.save(c);

	}
	@RequestMapping(value = "/articlesDelete/{id}", method = RequestMethod.POST)
	public boolean supprimer(@PathVariable Long id) {
		 try {
			 articleRepository.delete(id);
			 return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 return false;
		}
	

	}

}
