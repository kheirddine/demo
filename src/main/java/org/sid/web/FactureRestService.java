package org.sid.web;

import java.util.List;

import org.sid.dao.FactureRepository;
import org.sid.entities.Facture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class FactureRestService {
	
	@Autowired
	private FactureRepository factureRepository;

	@RequestMapping(value = "/factures", method = RequestMethod.GET)
	public List<Facture> getArticles() {
		return factureRepository.findAll();

	}
	 
	@RequestMapping(value = "/factures/{id}", method = RequestMethod.GET)
	public Facture getArticles(@PathVariable Long id) {
		return factureRepository.findOne(id);

	}
	@RequestMapping(value = "/factures", method = RequestMethod.POST)
	public Facture save(@RequestBody Facture c ) {
		return factureRepository.save(c);

	}
	@RequestMapping(value = "/facturesUp/{id}", method = RequestMethod.POST)
	public Facture update(@PathVariable Long id ,@RequestBody Facture c ) {
		c.setId(id);
		return factureRepository.save(c);

	}
	@RequestMapping(value = "/facturesDelete/{id}", method = RequestMethod.POST)
	public boolean supprimer(@PathVariable Long id) {
		 try {
			 factureRepository.delete(id);
			 return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 return false;
		}
	

	}

}
