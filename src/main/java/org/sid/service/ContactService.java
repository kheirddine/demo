package org.sid.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;
import org.sid.entities.Contact;
import org.sid.interfaces.IContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
public class ContactService implements IContactService,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IContactService contactService;
	
	

	@Transactional
	public Contact save(Contact Contact) {
		 return contactService.save(Contact);
	}

	@Transactional
	public void update(Contact Contact) {
		contactService.save(Contact);
	}

	@Transactional
	public void delete(Long id) {
		contactService.deleteById(id);
	}

	@Transactional
	public void deleteById(Long id) {
		contactService.deleteById(id);
	}

	@Transactional
	public void deleteAll() {
		contactService.deleteAll();
	}

	@Transactional
	public Contact getOne(Long id) {
		return contactService.getOne(id);
	}

	@Transactional
	public List<Contact> findAll() {
		return contactService.findAll();
	}

	@Transactional
	public void deleteAllList(List<Contact> listContacts) {
		contactService.deleteAllList(listContacts);
	}
	
	@Transactional
	public void saveAllList(List<Contact> listContacts) {
		contactService.saveAllList(listContacts);
	}
	
	@Transactional
	public Long count() {
		return contactService.count();
	}
	@Transactional
	@Override
	public Contact findByEmail(String email) {
		return contactService.findByEmail(email);
	}
	@Transactional
	@Override
	public Page<Contact> chercher(String string, PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return contactService.chercher(string, pageRequest) ;
	}
	@Transactional
	@Override
	public Contact findOne(Long id) {
		// TODO Auto-generated method stub
		return  contactService.findOne(id);
	}

	@Transactional
	@Override
	public Contact findByTypeUser(Integer idType) {
		// TODO Auto-generated method stub
		return contactService.findByTypeUser(idType);
	}


}
